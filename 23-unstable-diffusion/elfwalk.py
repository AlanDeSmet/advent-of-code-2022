#! /usr/bin/python3

import advent
import sys
import collections

PtTuple = collections.namedtuple("Pt", ["x","y"])

class Pt (PtTuple):
    def __add__(self, other):
        return Pt(self.x+other.x, self.y+other.y)

all_neighbors = []
for x in range(-1,+1+1):
    for y in range(-1,+1+1):
        if x == 0 and y == 0:
            continue
        all_neighbors.append(Pt(x,y))

Rule = collections.namedtuple("Rule", ["tests", "move"])

def load(f):
    elves = set()
    for y, line in enumerate(f):
        line = line.rstrip()
        for x, char in enumerate(line):
            assert char in "#.", f"Unexpected character '{char}'"
            if char == "#":
                pt = Pt(x,y)
                elves.add(pt)
    return elves

def find_extents(points):
    min_x = min([pt.x for pt in points])
    min_y = min([pt.y for pt in points])
    max_x = max([pt.x for pt in points])
    max_y = max([pt.y for pt in points])
    return Pt(min_x, min_y), Pt(max_x, max_y)

def find_extents_soft(points, soft_min, soft_max):
    if soft_min is None: soft_min = list(points)[0]
    if soft_max is None: soft_max = list(points)[0]
    min_pt, max_pt = find_extents(points)
    min_x = min(min_pt.x, soft_min.x)
    min_y = min(min_pt.y, soft_min.y)
    max_x = max(max_pt.x, soft_max.x)
    max_y = max(max_pt.y, soft_max.y)
    return Pt(min_x, min_y), Pt(max_x, max_y)

def print_2d_set(points, soft_min_extent=None, soft_max_extent=None, stream=sys.stdout):
    min_pt, max_pt = find_extents_soft(points, soft_min_extent, soft_max_extent)
    for y in range(min_pt.y, max_pt.y+1):
        out = ""
        for x in range(min_pt.x, max_pt.x+1):
            c = "."
            if Pt(x,y) in points:
                c = "#"
            out += c
        stream.write(out+"\n")

def print_2d_dict(points, soft_min_extent=None, soft_max_extent=None, stream=sys.stdout):
    min_pt, max_pt = find_extents_soft(points, soft_min_extent, soft_max_extent)
    for y in range(min_pt.y, max_pt.y+1):
        out = ""
        for x in range(min_pt.x, max_pt.x+1):
            out += str(points[Pt(x,y)])
        stream.write(out+"\n")


def is_elf_present(origin, elves, checks):
    for pt in checks:
        pt += origin
        if pt in elves:
            return True
    return False

def find_destination(elf, elves, rules):
    if not is_elf_present(elf, elves, all_neighbors):
        # Do nothing if no current neighbors
        return elf
    for rule in rules:
        if not is_elf_present(elf, elves, rule.tests):
            return rule.move + elf
    return elf

def plan(elves, rules):
    destinations = collections.defaultdict(int)
    for elf in elves:
        chosen_move = find_destination(elf, elves, rules)
        destinations[chosen_move] += 1
    return destinations

def move(elves, rules, destinations):
    new_elves = set()
    for elf in elves:
        chosen_move = find_destination(elf, elves, rules)
        if destinations[chosen_move] > 1:
            new_elves.add(elf)
            continue
        assert destinations[chosen_move] == 1
        new_elves.add(chosen_move)
    return new_elves

def count_empty(elves):
    num_elves = len(elves)
    min_pt, max_pt = find_extents(elves)
    width  = max_pt.x - min_pt.x + 1
    height = max_pt.y - min_pt.y + 1
    total_area = width * height
    num_empty = total_area - num_elves
    return num_empty


rules = [
        Rule( [Pt(-1,-1),Pt( 0,-1), Pt(+1,-1)], Pt( 0,-1)),
        Rule( [Pt(-1,+1),Pt( 0,+1), Pt(+1,+1)], Pt( 0,+1)),
        Rule( [Pt(-1,-1),Pt(-1, 0), Pt(-1,+1)], Pt(-1, 0)),
        Rule( [Pt(+1,-1),Pt(+1, 0), Pt(+1,+1)], Pt(+1, 0)),
        ]

