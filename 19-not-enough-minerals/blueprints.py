#! /usr/bin/python3

import sys
import re
from collections import namedtuple

START_BOTS = {"ore": 1, "clay": 0, "obsidian": 0, "geode": 0}

ResourceTuple = namedtuple("Resources", ["ore", "clay", "obsidian", "geode"])

class Resources(ResourceTuple):
    def __repr__(self):
        ret = f"{self.ore} ore"
        if self.clay > 0:
            ret += f", {self.clay} clay"
        if self.obsidian > 0:
            ret += f", {self.obsidian} obsidian"
        if self.geode > 0:
            ret += f", {self.geode} geode"
        return ret

    def __sub__(self, other):
        return Resources(self.ore - other.ore, self.clay - other.clay, self.obsidian - other.obsidian, self.geode - other.geode)



def read(stream):
    blueprints = []
    for line in stream:
        m = re.match(r'Blueprint (\d+): Each ore robot costs (\d+) ore. Each clay robot costs (\d+) ore. Each obsidian robot costs (\d+) ore and (\d+) clay. Each geode robot costs (\d+) ore and (\d+) obsidian.', line)
        fields = [int(x) for x in m.groups()]
        (blueprint_num,
                orebot_ore,
                claybot_ore,
                obsidianbot_ore, obsidianbot_clay,
                geodebot_ore, geodebot_obsidian
                ) = fields
        blueprint = {
                "ore":      Resources(orebot_ore,      0,   0, 0),
                "clay":     Resources(claybot_ore,     0,   0, 0),
                "obsidian": Resources(obsidianbot_ore, obsidianbot_clay, 0, 0),
                "geode":    Resources(geodebot_ore,    0,   geodebot_obsidian, 0),
                }
        blueprints.append(blueprint)
    return blueprints

def upcoming_choices(blueprint, bots, resources):
    for bottype, price in blueprint:
        needed = budget - price
        if needed.ore < 0:
            rnds = ceil(needed.ore / bots['ore'])




def can_afford(budget, recipe):
    return (budget.ore      >= recipe.ore      and
            budget.clay     >= recipe.clay     and
            budget.obsidian >= recipe.obsidian)

def find_affordable(budget, blueprint):
    ret = []
    for name, price in blueprint.items():
        if can_afford(budget, price):
            ret.append(name)
    return ret


def main():
    testdata="""Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.
"""
    import io
    blueprints = read(io.StringIO(testdata))
    assert len(blueprints) == 2
    assert blueprints[0]["ore"] == Resources(4,0,0,0)
    assert blueprints[0]["clay"] == Resources(2,0,0,0)
    assert blueprints[0]["obsidian"] == Resources(3,14,0,0)
    assert blueprints[0]["geode"] == Resources(2,0,7,0)
    assert blueprints[1]["ore"] == Resources(2,0,0,0)
    assert blueprints[1]["clay"] == Resources(3,0,0,0)
    assert blueprints[1]["obsidian"] == Resources(3,8,0,0)
    assert blueprints[1]["geode"] == Resources(3,0,12,0)

    assert can_afford(Resources(1,2,3,0), Resources(1,1,1,0))
    assert can_afford(Resources(2,2,2,0), Resources(1,1,1,0))
    assert not can_afford(Resources(2,2,2,0), Resources(3,1,1,0))
    assert not can_afford(Resources(2,2,2,0), Resources(1,3,1,0))
    assert not can_afford(Resources(2,2,2,0), Resources(1,1,3,0))

    def assert_eq(a,b):
        assert a==b, f"{a} != {b}"


    assert_eq( find_affordable(Resources(1,1,1,0), blueprints[0]), [])
    assert_eq( find_affordable(Resources(3,0,0,0), blueprints[0]), ["clay"])
    assert_eq( find_affordable(Resources(4,0,0,0), blueprints[0]), ["ore","clay"])
    assert_eq( find_affordable(Resources(2,0,6,0), blueprints[0]), ["clay"])
    assert_eq( find_affordable(Resources(2,0,7,0), blueprints[0]), ["clay", "geode"])
    assert_eq( find_affordable(Resources(4,13,0,0), blueprints[0]), ["ore","clay"])
    assert_eq( find_affordable(Resources(4,14,0,0), blueprints[0]), ["ore","clay", "obsidian"])
    assert_eq( find_affordable(Resources(4,14,12,0), blueprints[0]), ["ore","clay", "obsidian", "geode"])

    print("tests passed")



if __name__ == '__main__':
    sys.exit(main())
