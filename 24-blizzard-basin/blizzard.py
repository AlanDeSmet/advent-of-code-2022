#! /usr/bin/python3
import re
import collections

PtTuple = collections.namedtuple("Pt", ["x","y"])

class Pt (PtTuple):
    def __add__(self, other): return Pt(self.x+other.x, self.y+other.y)
    def __repr__(self):
        return f"({self.x},{self.y})"

UP   = Pt( 0,-1)
RIGHT= Pt(+1, 0)
DOWN = Pt( 0,+1)
LEFT = Pt(-1, 0)

class Cell:
    def __init__(self, up=False, right=False, down=False, left=False):
        self.up    = up
        self.right = right
        self.down  = down
        self.left  = left

    def offsets(self):
        ret = []
        if self.up:    ret.append(UP)
        if self.right: ret.append(RIGHT)
        if self.down:  ret.append(DOWN)
        if self.left:  ret.append(LEFT)
        return ret

    def simplechar(self):
        count = self.up+self.right+self.down+self.left
        if count == 0: return "."
        if count > 1: return str(count)
        if self.up: return "^"
        if self.right: return ">"
        if self.down: return "v"
        if self.left: return "<"
        assert False

    def nicechar(self):
        idx = self.up*1 + self.right*2 + self.down*4 + self.left*8
              #           111111
              # 0123456789012345
              #  ^ ^ ^ ^ ^ ^ ^ ^   
              #   >>  >>  >>  >>   
              #     vvvv    vvvv
              #         <<<<<<<<   
        return "·╵╶└╷│┌├╴┘─┴┐┤┬┼"[idx]
        #return "·🠹🠺⮤🠻⬍⮣├🠸⮠⬌┴⮧┤┬┼"[idx]

    def blocked(self):
        return self.up or self.right or self.down or self.left
    def clear(self):
        return not self.blocked()

def load(stream):
    first = next(stream)
    assert re.match(r"#\.#+$", first)
    width = len(first)

    lines = stream.read().rstrip().split("\n")
    expect = "#"*(width-2)+".#"
    assert lines[-1] == "#"*(width-3)+".#"
    lines = lines[:-1]

    grid = []
    for line in lines:
        row = []
        line = line[1:-1] # slice walls
        for cell in line:
            if   cell == "^": row.append(Cell(up   = True))
            elif cell == ">": row.append(Cell(right= True))
            elif cell == "v": row.append(Cell(down = True))
            elif cell == "<": row.append(Cell(left = True))
            else:             row.append(Cell())
        grid.append(row)

    return grid



def step(grid):
    newgrid = []
    height = len(grid)
    width = len(grid[0])
    for y in range(height):
        newrow = []
        for x in range(len(grid[y])):
            y_up    = (y-1)%height
            x_right = (x+1)%width
            y_down  = (y+1)%height
            x_left  = (x-1)%width
            up    = grid[y_down][x      ].up
            right = grid[y     ][x_left ].right
            down  = grid[y_up  ][x      ].down
            left  = grid[y     ][x_right].left
            newrow.append(Cell(up=up, right=right, down=down, left=left))
        newgrid.append(newrow)
    return newgrid


def as_strings(grid, fancy = False, E = None):
    ret = []

    if E is not None:
        if E.y<0:
            assert E == Pt(0,-1), f"Out of bounds at {E}"
        elif E.y==len(grid):
            assert E.x == len(grid[0])-1
        else:
            assert E.y >= 0 and E.y < len(grid), f"Out of bounds are {E}"
            assert E.x >= 0 and E.x < len(grid[0]), f"Out of bounds are {E}"


    firstrow = "#"
    if E == Pt(0,-1): firstrow += "E"
    else: firstrow += "."
    firstrow += "#"*len(grid[0])
    ret.append(firstrow)

    for y,row in enumerate(grid):
        out = "#"
        for x,cell in enumerate(row):
            if Pt(x,y) == E:
                assert cell.simplechar() == "."
                out += "E"
            elif fancy:
                out += cell.nicechar()
            else:
                out += cell.simplechar()
        out += "#"
        ret.append(out)

    lastrow = "#"*len(grid[0])
    if E == Pt(len(grid[0])-1,len(grid)): lastrow += "E"
    else: lastrow += "."
    lastrow += "#"
    ret.append(lastrow)

    return ret

def print_grid(grid, fancy = False, E = None):
    for line in as_strings(grid, fancy, E=E):
        print(line)

def as_multi_line_strings(grid):
    ret = []
    for row in grid:
        row1 = ""
        row2 = ""
        row3 = ""
        for cell in row:
            if cell.up: row1 += " ▲ "
            else:       row1 += "   "

            if cell.left:  row2 += "◀"
            else:          row2 += " "
            row2 += "·"
            if cell.right: row2 += "▶"
            else:          row2 += " "

            if cell.down:  row3 += " ▼ "
            else:          row3 += "   "
        ret += [row1, row2, row3]
    return ret

def print_grid_multiline(grid):
    for line in as_multi_line_strings(grid):
        print(line)


def main():
    import sys

    Epos = [
            Pt( 0,-1), #  0
            Pt( 0, 0), #  1
            Pt( 0,+1), #  2
            Pt( 0,+1), #  3
            Pt( 0, 0), #  4
            Pt(+1, 0), #  5
            Pt(+2, 0), #  6
            Pt(+2,+1), #  7
            Pt(+1,+1), #  8
            Pt(+1, 0), #  9
            Pt(+2, 0), # 10
            Pt(+2, 0), # 11
            Pt(+2,+1), # 12
            Pt(+2,+2), # 13
            Pt(+3,+2), # 14
            Pt(+4,+2), # 15
            Pt(+5,+2), # 16
            Pt(+5,+3), # 17
            Pt(+5,+4), # 18
            ]

    grid = load(open("test.input"))
    width = len(grid[0])
    height = len(grid)

    #sys.stdout.write("\33[2J")

    entrance = Pt(0,-1)
    exit = Pt(width,height+1)

    fancy = False

    #sys.stdout.write("\033[H")
    print("Initial state:")
    print_grid(grid, fancy, Epos[0])
    print()

    import time
    #sys.stdout.write("\33[2J")
    for minute, E in enumerate(Epos[1:]):
        #sys.stdout.write("\033[H")
        grid = step(grid)
        print(f"Minute {minute}, move ___:")
        print_grid(grid, fancy, E)
        print("")

    

if __name__ == '__main__':
    import sys
    sys.exit(main())

