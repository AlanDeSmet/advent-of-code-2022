#! /usr/bin/python3

import sys

class directory:
    def __init__(self, name, parent=None):
        self.name = name
        self.parent = parent
        self.subdirs = {}
        self.xlocalsize = 0

    def add_file(self, name, size):
        self.xlocalsize += size

    def add_subdir(self, name):
        newdir = directory(name, self)
        self.subdirs[name] = newdir

    def get_subdir(self, name):
        assert name in self.subdirs
        return self.subdirs[name]

    def localsize(self): return self.xlocalsize
    def totalsize(self): 
        size = self.localsize()
        for subdir in self.subdirs.values():
            size += subdir.totalsize()
        return size


    def fullpath(self):
        if self.parent != None:
            return self.parent.fullpath()+self.name+"/"
        return self.name

    def print(self, depth=0):
        print("  "*depth+self.name, self.localsize())
        for name in sorted(self.subdirs):
            subdir = self.subdirs[name]
            subdir.print(depth+1)

    def visit(self, visitor):
        visitor.visit(self)
        for name in sorted(self.subdirs):
            subdir = self.subdirs[name]
            subdir.visit(visitor)

class CollectSmallSizes:
    def __init__(self):
        self.totalsize = 0
    def visit(self, directory):
        size = directory.totalsize()
        if size <= 100000:
            self.totalsize += size

class FindSmallestTarget:
    def __init__(self, minsize):
        self.minsize = minsize
        self.smallestsize = 99999999999999
    def visit(self, directory):
        size = directory.totalsize()
        if size > self.minsize and size < self.smallestsize:
            self.smallestsize = size

def read_command_stream(filename):
    f = open(filename)
    lines = list([x.rstrip() for x in f.readlines()])

    idx = 0

    def read_cmd():
        nonlocal idx
        if idx > (len(lines)-1):
            return None, None
        line = lines[idx]
        idx += 1
        parts = line.split()
        assert len(parts) >= 2, f"Command lacks a command: {line}"
        assert len(parts) <= 3, f"Command has too many arguments: {line}"
        assert parts[0] == "$", f"Command didn't start with $, instead {parts[0]}"
        cmd = parts[1]
        assert cmd in ["ls","cd"], f"Unknown command {cmd}"
        newdir = None
        if len(parts) == 3:
            newdir = parts[2]
        return cmd, newdir


    cmd, newdir = read_cmd()
    assert cmd == "cd" and newdir == "/", f"expected 'cd /', found '{cmd} {newdir}'"

    curdir = directory("/")

    while True:
        cmd, arg = read_cmd()
        if cmd == None: break;

        if cmd == "ls":
            while idx < len(lines) and lines[idx][0] != "$":
                size,name = lines[idx].split()
                if size == "dir":
                    curdir.add_subdir(name)
                    #print(curdir.fullpath(), "adding subdir", name)
                else:
                    assert size[0] in "0123456789"
                    curdir.add_file(name, int(size))
                idx += 1;
        if cmd == "cd":
            if arg == "..":
                curdir = curdir.parent
                assert curdir != None
            else:
                curdir = curdir.get_subdir(arg)

    while curdir.parent != None:
        curdir = curdir.parent
    return curdir

def main():
    disk_size = 70_000_000
    required_space = 30_000_000
    allowed_space = disk_size - required_space

    dirtree = read_command_stream("input")

    used_space = dirtree.totalsize()
    min_delete = used_space - allowed_space
    print(f"Need to free {min_delete} bytes")

    finder = FindSmallestTarget(min_delete)
    dirtree.visit(finder)
    print(finder.smallestsize)


if __name__ == '__main__':
    sys.exit(main())
